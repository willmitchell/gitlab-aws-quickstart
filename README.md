# GitLab AWS Quickstart

This repository contains a CloudFormation template to set up a GitLab deployment
on AWS.

## Status - In development

This cloud formation template is **being actively developed**, and should not be
used for production systems as it will change and may contains bugs.

These are rough instructions intended to help developers get started with the CFN
template, and assumes a level of familiarity with CFN. Once development is complete,
documentation will be created and made available.

## Getting started

### Create an S3 Bucket to hold assets

To run this CloudFormation template you need to create your own S3 Bucket to
which you need to upload the assets to. This sums up to 4 steps:

1. Create an S3 Bucket (or use preexisting one), i.e. `gitlab-quickstart-bucket`
2. Have an IAM user which has access to that Bucket
2. Clone this repo or download [the zip file](https://gitlab.com/gitlab-org/gitlab-aws-quickstart/repository/master/archive.zip)
   which contains the quick start assets
3. Upload all assets to that bucket

Let’s assume that you have created the bucket `gitlab-quickstart-bucket` and you are in
the directory with the unzipped assets:

```bash
$ ls

submodules
templates
```

Now you need to upload those assets to your S3 bucket either by dragging and
dropping them in the AWS console or using the `aws` CLI:

```bash
aws --profile gitlab-quickstart s3 cp . s3://gitlab-quickstart-bucket/gitlab-quickstart --recursive
```

This will upload the assets in a `gitlab-quickstart` directory inside the bucket
you created.

Note that bucket name as you will need to use it in the "Quick Start S3 Bucket
Name" parameter under the "Section AWS Quick Start Configuration", when
[configuring the template](#configure-the-template).

**Note:** if you upload assets to a different path than `gitlab-quickstart`,
then you need to change also the "Quick Start S3 Key Prefix" when
[configuring the template](#configure-the-template).

### Launch CloudFormation

Launch CloudFormation and use an S3 link to your template, for example:

```
https://s3.amazonaws.com/gitlab-quick-start-testing-us-east-1/gitlab-quickstart/templates/gitlab-master.template
```

There are two templates to choose from:

- `gitlab-master.template` - launches GitLab Quick Start in a new VPC
- `gitlab.template` - launches GitLab Quick Start in an existing VPC

### Configure the template

Enter the desired settings for the template. A few helpful notes:

- Do not change the **VPCDefinition**. If you do, it expects a map defining the
  new definition.
- The "Quick Start S3 Bucket Name" needs to match the bucket name of the S3
  bucket you are using.
- We recommend to turn "Rollback on failure" to **no** in the advanced options after
  specifying the parameters. This is so you have a chance to debug any issues
  that arise during provisioning (and useful for development).
- Keep in mind the Route 53 name needs to include the final '.' at the end of
  the name.

### Deploy and access your GitLab instance

When all is complete, go into the "Outputs" section of CloudFormation stack and
you will see a link with which you can access GitLab.
