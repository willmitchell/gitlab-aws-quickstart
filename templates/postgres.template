{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "Creates a Postgres RDS instance within a VPC.  **WARNING** This template creates an Amazon RDS instance of the size and instance type that you specify. You will be billed for the AWS resources used if you create a stack from this template.",
    "Parameters": {
        "VpcId": {
            "Description": "VPC id.",
            "Type": "AWS::EC2::VPC::Id"
        },
        "VpcCIDR": {
            "Description": "VPC CIDR",
            "Type": "String"
        },
        "PrivateSubnetID1": {
            "Description": "RDS subnet id1.",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "PrivateSubnetID2": {
            "Description": "RDS subnet id2.",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "RDSUsername": {
            "Description": "The user name that is associated with the master user account for the RDS that is being created",
            "Type": "String"
        },
        "RDSPassword": {
            "Description": "The password that is associated with the master user account for the RDS that is being created.",
            "Type": "String",
            "NoEcho": "true"
        },
        "RDSDatabaseName": {
            "Description": "Database name",
            "Type": "String"
        },
        "RDSInstanceType": {
            "Description": "Instance type",
            "Type": "String",
            "AllowedValues": [
                "db.t2.micro",
                "db.t2.small",
                "db.t2.medium",
                "db.t2.large"
            ]
        },
        "RDSStorageSize": {
            "Description": "RDS storage size",
            "Type": "String",
            "NoEcho": "true"
        },
        "RDSPort": {
            "Description": "Instance port",
            "Type": "Number"
        },
        "MultiAZDatabase": {
            "Default": "true",
            "Description": "Create a multi-AZ RDS database instance",
            "Type": "String",
            "AllowedValues": [
                "true",
                "false"
            ],
            "ConstraintDescription": "must be either true or false"
        }
    },
    "Resources": {
        "RDSInstance" : {
            "Type" : "AWS::RDS::DBInstance",
            "Properties" : {
                "DBName": { "Ref": "RDSDatabaseName" },
                "AllocatedStorage" : { "Ref": "RDSStorageSize"},
                "DBInstanceClass" : {"Ref": "RDSInstanceType" },
                "Engine" : "postgres",
                "Port": { "Ref": "RDSPort" },
                "EngineVersion" : "9.6.2",
                "MasterUsername" : { "Ref" : "RDSUsername" },
                "MasterUserPassword" : { "Ref" : "RDSPassword" },
                "DBSubnetGroupName" : { "Ref" : "RDSSubnetGroup" },
                "VPCSecurityGroups" : [ { "Ref" : "RDSSecurityGroup" }  ],
                "PubliclyAccessible": "false",
                "AutoMinorVersionUpgrade": "false",
                "MultiAZ": { "Ref": "MultiAZDatabase" },
                "Tags" : [ { "Key" : "Name", "Value" : "GitlabQuickstart" } ]
            }
       },
       "RDSSubnetGroup": {
            "Type": "AWS::RDS::DBSubnetGroup",
            "Properties": {
                "DBSubnetGroupDescription": "RDS subnet group",
                "SubnetIds": [
                    {
                        "Ref": "PrivateSubnetID1"
                    },
                    {

                        "Ref": "PrivateSubnetID2"
                    }
                ]
            }
        },
        "RDSSecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "VpcId": {
                    "Ref": "VpcId"
                },
                "GroupDescription": "Enable Postgres port",
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": { "Ref": "RDSPort" },
                        "ToPort": { "Ref": "RDSPort" },
                        "CidrIp": { "Ref": "VpcCIDR" }
                    }
                ]
            }
        }
    },
    "Outputs": {
        "RDSHostname": {
            "Description": "RDS hostname",
            "Value": {
                "Fn::GetAtt": [
                    "RDSInstance",
                    "Endpoint.Address"
                ]
            }
        },
        "RDSPort": {
            "Description": "RDS port",
            "Value": {
                "Fn::GetAtt": [
                    "RDSInstance",
                    "Endpoint.Port"
                ]
            }
        }
    }
}